WARNING!

When using the ddump repository, you agree that the file(s) sent from your machine will be visible on a public website and automatically re-licenced as public domain material.

Make sure that you own the right of the file(s) you contribute to the repository.

