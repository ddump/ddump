#!/usr/bin/python
import subprocess
from subprocess import Popen, PIPE, STDOUT
import time
import os,sys

verbose = ""

for options in sys.argv:
    if options == "--verbose":
        verbose = True

dd_path = os.path.expanduser("~")+'/.local/share/ShareTrash/'

def check_netw():
	cmd = "ping -c 2 pzwart2.wdka.hro.nl"
	p = Popen(cmd, shell=True, stdout=PIPE, close_fds=True)
	output = p.stdout.read()
	return output
				

def clean_name():
	for root, dirs, files in os.walk(dd_path):
		for file in files:
			if "(" in file or ")" in file or " " in file:
				print file
				print root
				new_file = file.replace("(", "")
				new_file = new_file.replace(")", "")
				new_file = new_file.replace(" ", "")
				os.rename(root + "/" + file, root + "/" + new_file)


dd_files = []

def get_files():
		cmd = "rsync --no-motd --password-file /usr/share/doc/ddump/.config/password ddumper@pzwart2.wdka.hro.nl::public/files"
		p = Popen(cmd, shell=True, stdout=PIPE, close_fds=True)
		output = p.stdout.read()
		stdout = output.split(' ')
		for element in stdout:
			if '\n' in element:
				new_e = element.split('\n')
				dd_files.append(new_e[0])		


def rename(e,list):
	if e in list:
			num = 0
			newnum = 0
			while True:
					num += 1
					(base, ext) = os.path.splitext(e)
					if base.split('_')[:-1]:
							(newbase, newnum) = base.split('_')
							(base, num) = (newbase, int(newnum) + 1)
					f = base + '_' + ("%04d"%num) + ext
					if not f in list:
							return f
							break   
					e = f
	else:
		return e

while True:
	output = check_netw()
	if output == "":
		print "not connected!"
		time.sleep(30)
	else:
		print "connected!"
		get_files()
		clean_name()
		for file in os.listdir(dd_path):
			if file != "Icon\r" and file != "Icon^M" and file != "Icon?" and file != "Icon#*":
				newfile = rename(file, dd_files)
				print file, newfile
				subprocess.call("rsync -r --password-file /usr/share/doc/ddump/.config/password -Cav '" + os.path.join(dd_path, file) + "' 'ddumper@pzwart2.wdka.hro.nl::public/files/" + newfile + "'", shell=True)
				subprocess.call("rm -r '" + os.path.join(dd_path, file) + "'", shell=True)
				print 'sent'
		
		time.sleep(30)
