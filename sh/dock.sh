#!/bin/sh
# got root?
if [[ $(id -g) != 0 ]]
then
    echo "not root"
    exit 0
fi 

# Install Icon for every users
for ddumpuser in $(users)
do
	DOCKTRASH=`sudo grep ShareTrash /Users/$ddumpuser/Library/Preferences/com.apple.dock.plist`
    if [ "$DOCKTRASH" == "" ]
    then 
        echo "no trash found in dock"
        echo "Installing ddump folder for ${ddumpuser}"
        if [ ! -d "/Users/$ddumpuser/.local/share/" ]
        then
            su $ddumpuser -c "mkdir -p /Users/$ddumpuser/.local/share/"
            su $ddumpuser -c "mkdir -p /Users/$ddumpuser/.config/ddump/"
        fi
        echo "setting password file"
        su $ddumpuser -c "echo $PASSWORD > /Users/$ddumpuser/.config/ddump/password"
        su $ddumpuser -c "chmod 660 /Users/$ddumpuser/.config/ddump/password"
        su $ddumpuser -c "cp -r /usr/share/doc/ddump/ShareTrash /Users/$ddumpuser/.local/share/" 
        su $ddumpuser -c "defaults write com.apple.dock persistent-others -array-add '<dict><key>tile-data</key><dict><key>file-data</key><dict><key>_CFURLString</key><string>'/Users/$ddumpuser/.local/share/ShareTrash'</string><key>_CFURLStringType</key><integer>0</integer></dict></dict><key>tile-type</key><string>'directory-tile'</string></dict>'"
    else
        echo "trash found in dock"
    fi
done
    
killall Dock

