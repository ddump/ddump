#!/bin/sh

if [ "$(id -u)" != "0" ]
then
  echo "You are not root"
  echo "Please try sudo"
  exit 1
fi 

cp ../re/re /usr/local/bin
cp ../ddumpd/ddumpd.py /usr/local/bin
