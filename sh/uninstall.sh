#!/bin/sh

if [ $UID != "0" ]
then
  echo "You are not root"
  echo "Please try sudo"
  exit 1
fi 

rm /usr/local/bin/re
rm /usr/local/bin/ddumpd.py
