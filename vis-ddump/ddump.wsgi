import os, sys

sys.path.append("/home/lbontempi/wsgi")
sys.path.append("/home/lbontempi/wsgi/ddump")
os.environ['DJANGO_SETTINGS_MODULE'] = 'ddump.settings'
import django.core.handlers.wsgi
application = django.core.handlers.wsgi.WSGIHandler()
