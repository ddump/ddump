from django.conf.urls.defaults import *
from django.conf import settings


# Uncomment the next two lines to enable the admin:
from django.contrib import admin
admin.autodiscover()

urlpatterns = patterns('',
    # Example:
    # (r'^yellowstone/', include('yellowstone.foo.urls')),	
    #(r'^wsite/$', 'wsite.views.postList'),
    #(r'^ddump/home/$', 'viz.views.viz1'),
    (r'^ddump/home/$', 'viz.views.vis_a'),
    (r'^ddump/home/filter/(?:(?P<filter>.+)/)?$', 'viz.views.vis_a'),
    (r'^ddump/home/info/(?:(?P<filter>.+)/)?$', 'viz.views.vis_b'),
    (r'^ddump/home/tab/(?:(?P<filter>.+)/)?$', 'viz.views.vis_c'),
    (r'^ddump/home/list/(?:(?P<filter>.+)/)?$', 'viz.views.vis_list'),
    (r'^ddump/home/post/$', 'viz.views.post'),
    (r'^ddump/home/about/$', 'viz.views.vis_about'),
    (r'^ddump/home/projects/$', 'viz.views.vis_projects'),
    (r'^ddump/home/software/$', 'viz.views.vis_software'),
    # Uncomment the admin/doc line below and add 'django.contrib.admindocs' 
    # to INSTALLED_APPS to enable admin documentation:
    # (r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
     (r'^admin/', include(admin.site.urls)),
)

if settings.LOCAL_DEV:
    baseurlregex = r'^static/(?P<path>.*)$'
    urlpatterns += patterns('',
        (baseurlregex, 'django.views.static.serve',
        {'document_root':  settings.MEDIA_ROOT}),
    )


