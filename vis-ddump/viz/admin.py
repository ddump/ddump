from ddump.viz.models import Dfile, Tag, Out
from django.contrib import admin

class DfileAdmin(admin.ModelAdmin):
    list_display = ("title", "dfile", "size", "pub_date","mod_date", "dtype", "admin_tags",)
    search_fields = ["dfile", "dtype"]
    filter_horizontal = ["tags"]

admin.site.register(Dfile, DfileAdmin)
admin.site.register(Tag)
admin.site.register(Out)

