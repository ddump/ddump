from django.db import models

class Tag(models.Model):
    class Meta ():
        ordering = ["name"]
    name = models.CharField(max_length=200, blank = True, null = True)
    group = models.CharField(max_length=200, blank = True, null = True)    
    def __unicode__(self):
        return self.name

class Out(models.Model):
    class Meta ():
        ordering = ["pub_date"]
    title = models.CharField(max_length=200, blank = True, null = True)
    pub_date = models.DateTimeField(verbose_name='date published', null = True, blank = True)
    dfile = models.FileField(upload_to="out")
    o_short = models.CharField(max_length=200, blank = True, null = True)
    o_long = models.CharField(max_length=500, blank = True, null = True)

    def __unicode__(self):
        return self.title

class Dfile(models.Model):
    class Meta ():
        ordering = ["pub_date"]
    title = models.CharField(max_length=255)
    dfile = models.FileField(upload_to="files")
    size = models.IntegerField(null = True, blank = True)
    pub_date = models.DateTimeField(verbose_name='date published', null = True, blank = True)
    mod_date = models.DateTimeField(verbose_name='date modified', null = True, blank = True)
    dtype = models.CharField(max_length=255, blank = True)
    file_x = models.IntegerField(null = True, blank = True)
    file_y = models.IntegerField(null = True, blank = True)
    tags = models.ManyToManyField(Tag)

    def __unicode__(self):
        return self.title

    def admin_tags(self):
        return ", ".join([t.name for t in self.tags.all()])