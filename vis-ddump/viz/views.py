from django.shortcuts import render_to_response
from django.template import RequestContext
from django.conf import settings
from models import *
import mimetypes, glob, httplib, fpformat, os, time, datetime, Image
from operator import itemgetter
from django.utils.datastructures import SortedDict
from django.template import Context, loader
from django.http import HttpResponse

def thumb(path):
    image = Image.open(path)
    file, ext = os.path.splitext(path)
    (directory, name) = os.path.split(path)
    box = (80, 50)
    margin_w = image.size[0]/4
    margin_h = image.size[1]/4
    x1 = margin_w
    y1 = margin_h
    x2 = image.size[0] - margin_w
    y2 = image.size[1] - margin_h
    image = image.crop((x1, y1, x2, y2))
    if image.size[0] > image.size[1] or image.size[0] == image.size[1] :
        h = box[1]
        w = float(image.size[0])/float(image.size[1])*box[1]
        image = image.resize((int(w), int(h)), Image.ANTIALIAS)
        while image.size[1] < 30:
            image = image.resize((image.size[0]*2, image.size[1]*2), Image.ANTIALIAS)
    else: 
        box = (50, 80)
        h = box[1]
        w = float(image.size[0])/float(image.size[1])*box[1]
        image = image.resize((int(w), int(h)), Image.ANTIALIAS)
        while image.size[0] < 30:
            image = image.resize((image.size[0]*2, image.size[1]*2), Image.ANTIALIAS)
    if image.size[0] > image.size[1] or image.size[0] == image.size[1] :
        if image.size[0] > 80:
            x2 = 80
            image = image.crop((0, 0, x2, image.size[1]))
    else:
        if image.size[1] > 80:
            y2 = 80
            image = image.crop((0, 0, image.size[0], y2))
    out = os.path.join(settings.MEDIA_ROOT, "thumb", name)
    image.save(out + "_thumb", "JPEG")

def sync():
    files_path = os.path.join(settings.MEDIA_ROOT, "files")
    for root, dirs, files in os.walk(files_path):
        for name in files:
            fpath = os.path.join(root, name)
            ppath = fpath.replace(settings.MEDIA_ROOT, "")
            (directory, filename) = os.path.split(fpath)
            (obj, created) = Dfile.objects.get_or_create(title = filename, dfile = ppath)
            if created:
                filesize = os.path.getsize(fpath)
                filesize = int(filesize) / 1024.0
                size = fpformat.fix(filesize,0)
                #if filesize >= 1024:
                    #filesize = int(filesize) / 1024.0
                    #size = fpformat.fix(filesize,0) + " MB"
                obj.size = size
                obj.pub_date = datetime.datetime.fromtimestamp(os.stat(fpath).st_atime)
                obj.mod_date = datetime.datetime.fromtimestamp(os.stat(fpath).st_mtime)
                dtype = str(mimetypes.guess_type(fpath)[0])
                if dtype != "image/gif" and dtype != "image/x-photoshop" and dtype != "image/svg+xml":
                    dtype_c = dtype.split('/')
                    obj.dtype = dtype_c[0]
                else:
                    obj.dtype = dtype
                obj.save()
                if obj.dtype == "image/jpeg" or obj.dtype == "image/png" or obj.dtype == "image/x-ms-bmp":
                    thumb(fpath)
             

def sortDict(d):
    sorted(d.items())
    sorted(d.items(), reverse=True)
    l = sorted(d.items(), key=itemgetter(1))
    sd = SortedDict()
    n = 0
    kl = []
    vl = []
    for tup in l:
        k = l[n][0]
        v = l[n][1]
        kl.append(k)
        vl.append(v)
        sd[kl[n]] = vl[n]
        n += 1
    return sd

def vis_a(request, filter=None):
    sync()
    dfiles = Dfile.objects.all()

    return render_to_response('index.html', {"dfiles":dfiles}, context_instance=RequestContext(request))

def vis_af(request, filter=None):
    sync()
    if filter == None:
        dfiles = Dfile.objects.all().reverse()[:200]
    elif filter == "image":
        dfiles = Dfile.objects.filter(dtype__in=('image/jpeg','image/png','image/x-ms-bmp','image/gif')).reverse()[:200]
    elif filter == "text":
        dfiles = Dfile.objects.filter(dtype__in=('text/plain', 'application/pdf',"text/rtf")).reverse()[:200]
    elif filter == "sound":
        dfiles = Dfile.objects.filter(dtype__in=('audio/x-wav','application/x-flac',"audio/mpeg","audio/x-aiff")).reverse()[:200]
    elif filter == "video":
        dfiles = Dfile.objects.filter(dtype__in=('video/mp4','video/quicktime',"application/x-shockwave-flash")).reverse()[:200]
    elif filter == "source":
        dfiles = Dfile.objects.filter(dtype__in=("application/postscript","image/svg+xml","image/x-photoshop","image/svg+xml")).reverse()[:200]
    elif filter == "code":
        dfiles = Dfile.objects.filter(dtype__in=("text/x-sh","text/x-python", "application/xml","text/x-c++src")).reverse()[:200]
    elif filter == "other":
        dfiles = Dfile.objects.filter(dtype__in=("None","application/vnd.ms-powerpoint","application/zip")).reverse()[:200]

    return render_to_response('index.html', {"dfiles":dfiles}, context_instance=RequestContext(request))

def vis_b(request, filter=None):
    sync()
    if filter == None:
        dfiles = Dfile.objects.all().reverse()[:300]
    elif filter == "image":
        dfiles = Dfile.objects.filter(dtype__in=('image/jpeg','image/png','image/x-ms-bmp','image/gif')).reverse()[:200]
    elif filter == "text":
        dfiles = Dfile.objects.filter(dtype__in=('text/plain', 'application/pdf',"text/rtf")).reverse()[:200]
    elif filter == "sound":
        dfiles = Dfile.objects.filter(dtype__in=('audio/x-wav','application/x-flac',"audio/mpeg","audio/x-aiff")).reverse()[:200]
    elif filter == "video":
        dfiles = Dfile.objects.filter(dtype__in=('video/mp4','video/quicktime',"application/x-shockwave-flash")).reverse()[:200]
    elif filter == "source":
        dfiles = Dfile.objects.filter(dtype__in=("application/postscript","image/svg+xml","image/x-photoshop","image/svg+xml")).reverse()[:200]
    elif filter == "code":
        dfiles = Dfile.objects.filter(dtype__in=("text/x-sh","text/x-python", "application/xml","text/x-c++src")).reverse()[:200]
    elif filter == "other":
        dfiles = Dfile.objects.filter(dtype__in=("None","application/vnd.ms-powerpoint","application/zip")).reverse()[:200]

    f1 = []
    cols = {}
    y_all={}

    today = datetime.date.today()
    big_year= 0
    small_year= today.year

    for dfile in dfiles:
        year = dfile.mod_date.year
        if year > big_year:
            big_year = year
        if year < small_year:
            small_year = year
        
    y_range = ((big_year-small_year)+1)*12
    month_px = 1100/y_range
    top = 425

    for dfile in dfiles:
        year = dfile.mod_date.year
        years = year - small_year
        months = dfile.mod_date.month
        tabs = (years*12)+months
        left = 50+(tabs*month_px)
        if left not in cols:
            cols[left]=1
        else:
            cols[left]=cols[left]+1
        pleft = 80+(month_px*(years*12))
        if year not in y_all:
            y_all[year]=pleft
        if top > 10:
            f1.append({
                'bottom':80+cols[left]*10,
                'left':left,
                'dfile':dfile,
                })

    context= {"dfiles":dfiles,"f1":f1,}
    context['y_all'] = y_all

    return render_to_response('info.html',context , context_instance=RequestContext(request))

def vis_c(request, filter=None):
    sync()

    try:
        tag = request.GET['tag']
    except:
        tag=''

    try:
        filt = request.GET['filt']
    except:
        filt=''

    if filter == None:
        dfiles = Dfile.objects.all().reverse()
    elif filter == "image":
        dfiles = Dfile.objects.filter(dtype__in=('image', 'image/gif')).reverse()
    elif filter == "text":
        dfiles = Dfile.objects.filter(dtype='text').reverse()
    elif filter == "audio":
        dfiles = Dfile.objects.filter(dtype='audio').reverse()
    elif filter == "video":
        dfiles = Dfile.objects.filter(dtype='video').reverse()
    elif filter == "app":
        dfiles = Dfile.objects.filter(dtype__in=("application","image/svg+xml","image/x-photoshop")).reverse()
    elif filter == "other":
        dfiles = Dfile.objects.filter(dtype="None").reverse()
    elif filter == "out of focus":
        dfiles = Dfile.objects.filter(tags__name="out of focus")
    elif filter == "underexposed":
        dfiles = Dfile.objects.filter(tags__name="underexposed")
    elif filter == "overexposed":
        dfiles = Dfile.objects.filter(tags__name="overexposed")
    elif filter == "missed subject":
        dfiles = Dfile.objects.filter(tags__name="missed subject")
    elif filter == "obscure subject":
        dfiles = Dfile.objects.filter(tags__name="obscure subject")
    elif filter == "backlight":
        dfiles = Dfile.objects.filter(tags__name="backlight")
    elif filter == "finger lens":
        dfiles = Dfile.objects.filter(tags__name="fingr lens")
    elif filter == "moving":
        dfiles = Dfile.objects.filter(tags__name="moving")
    elif filter == "boring":
        dfiles = Dfile.objects.filter(tags__name="boring")

    tab = []
    l1 = []
    i = 0
    c = 0
    x = 35
    y = 85   
    r=1
    for dfile in dfiles:
        if r < 2:
            if c < 2:
                if c == 0:
                    x = 125
                    x = x + 90
                else:
                    x = x + 90
            else:
                x = x + 90
            dtab = (dfile, x)
            l1.append(dtab)
            c = c + 1

            if c > 10:
                r = r+1
                dlist = (l1,y)
                tab.append(dlist)
                c = 0
                i = i + 1
                y = y + 87
                x = 35
                l1 = []
            else:
                dlist = (l1,y)
                tab.append(dlist)
                l1 = []
        else:
            if c > 0:
                x = x + 90
            dtab = (dfile, x)
            l1.append(dtab)
            c = c + 1

            if c > 12:
                r = r+1
                dlist = (l1,y)
                tab.append(dlist)
                c = 0
                i = i + 1
                y = y + 87
                x = 35
                l1 = []
            else:
                dlist = (l1,y)
                tab.append(dlist)
                l1 = []
            
    
    if request.method == "POST":
        fileids = request.POST.getlist('tagged')
        tagbutton = request.POST.get('tagbutton')
        for fid in fileids:
            tagobj = Tag.objects.get(name=tagbutton)
            df = Dfile.objects.get(pk=fid)
            df.tags.add(tagobj)

    context = {"dfiles":dfiles, "tab":tab, }
    context['tag_audio'] = Tag.objects.filter(group='audio')
    context['tag_image'] = Tag.objects.filter(group='image')
    context['tag_app'] = Tag.objects.filter(group='app')
    context['tag_video'] = Tag.objects.filter(group='video')
    context['tag_text'] = Tag.objects.filter(group='text')
    context['out'] = Out.objects.all()
    context['filt'] = filt
    context['tag'] = tag
    context['out_latest'] = Out.objects.latest('pub_date')

    return render_to_response('tab.html', context, context_instance=RequestContext(request))

def vis_list(request, filter=None):
    sync()
    if filter == None:
        dfiles = Dfile.objects.all().order_by("pub_date").reverse()
    elif filter == "image":
        dfiles = Dfile.objects.filter(dtype__in=('image/jpeg','image/png','image/x-ms-bmp','image/gif')).order_by("pub_date").reverse()
    elif filter == "text":
        dfiles = Dfile.objects.filter(dtype__in=('text/plain', 'application/pdf',"text/rtf")).reverse()
    elif filter == "sound":
        dfiles = Dfile.objects.filter(dtype__in=('audio/x-wav','application/x-flac',"audio/mpeg","audio/x-aiff")).order_by("pub_date").reverse()
    elif filter == "video":
        dfiles = Dfile.objects.filter(dtype__in=('video/mp4','video/quicktime',"application/x-shockwave-flash")).reverse()
    elif filter == "source":
        dfiles = Dfile.objects.filter(dtype__in=("application/postscript","image/svg+xml","image/x-photoshop","image/svg+xml")).order_by("pub_date").reverse()
    elif filter == "code":
        dfiles = Dfile.objects.filter(dtype__in=("text/x-sh","text/x-python", "application/xml","text/x-c++src")).order_by("pub_date").reverse()
    elif filter == "other":
        dfiles = Dfile.objects.filter(dtype__in=("None","application/vnd.ms-powerpoint","application/zip")).order_by("pub_date").reverse()

    return render_to_response('list.html', {"dfiles":dfiles, "list":list}, context_instance=RequestContext(request))

def vis_about(request):
    dfiles = Dfile.objects.all()
    return render_to_response('about.html', {"dfiles":dfiles}, context_instance=RequestContext(request))

def vis_projects(request):    
    dfiles = Dfile.objects.all()
    return render_to_response('projects.html', {"dfiles":dfiles}, context_instance=RequestContext(request))

def vis_software(request):    
    dfiles = Dfile.objects.all()
    return render_to_response('software.html', {"dfiles":dfiles}, context_instance=RequestContext(request))

def post(request):
    posx = request.POST['left']
    posy = request.POST['top']
    did = request.POST['id']
    dfile = Dfile.objects.get(id=did)
    dfile.file_x = posx
    dfile.file_y = posy
    dfile.save()
        
    return HttpResponse("ok")
